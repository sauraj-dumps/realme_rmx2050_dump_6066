## qssi-user 10 QKQ1.200209.002 0330_202107231050 release-keys
- Manufacturer: realme
- Platform: trinket
- Codename: RMX2050
- Brand: realme
- Flavor: qssi-user
- Release Version: 10
- Id: QKQ1.200209.002
- Incremental: 0330_202107231050
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: RMX2050EX_11.A.33_0330_202107231050
- Branch: RMX2050EX_11.A.33_0330_202107231050
- Repo: realme_rmx2050_dump_6066


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
